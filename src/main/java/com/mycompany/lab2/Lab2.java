/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab2;

import java.util.Scanner;

/**
 *
 * @author natta
 */
public class Lab2 {
    static char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'}
    };
    static char currentPlayer = 'X';
    static int row, col;

    static void printWelcome() {
        System.out.println("Welcome to OX");
    }

    static void printTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println();
        }
    }

    static void printTurn() {
        System.out.println(currentPlayer + "'s Turn");
    }

    static void inputRowCol(){
        while(true){
            Scanner kb = new Scanner(System.in);
            System.out.print("Please input row col : ");
            row = kb.nextInt();
            col = kb.nextInt();
            if(table[row-1][col-1]=='-'){
                table[row-1][col-1] = currentPlayer;
                break;
            }
        }    
    }
    
    static void switchPlayer(){
        if(currentPlayer=='X'){
            currentPlayer='O';
        }else{
            currentPlayer='X';
        }
    }
    static boolean isWin(){
        if(checkRow()||checkCol()||checkDiagonal1()||checkDiagonal2()){
            return true;
        }
        return false;
    }
    static boolean isDraw(){
        int blank = 0;
        for(int i=0; i<3; i++){
            for(int j=0; j<3; j++){
                if(table[i][j]!='-'){
                    blank+=1;
                }
            }
        }
        if(blank==9){
            return true;
        }
        return false;
    }
    
    static boolean checkRow(){
        for(int i=0; i<3; i++){
            if(table[row-1][i]!=currentPlayer){
                return false;
            } 
        }
        return true;
    }
        
    static boolean checkCol(){
        for(int i=0; i<3; i++){
            if(table[i][col-1]!=currentPlayer){
                return false;
            }
        }
        return true;  
    }
    
        static boolean checkDiagonal1(){
        if(table[0][0]==currentPlayer&&table[1][1]==currentPlayer&&table[2][2]==currentPlayer){
            return true;
        }
        return false;
    }
    
    static boolean checkDiagonal2(){
        if(table[0][2]==currentPlayer&&table[1][1]==currentPlayer&&table[2][0]==currentPlayer){
            return true;
        }
        return false;
    }
  
    static void showResult(){
        if(isDraw()){
            System.out.println("Draw!");
        }else{
            System.out.println(currentPlayer+" win!");
        }

    }
           
    static boolean inputContinue(){
        Scanner kb = new Scanner(System.in);
        System.out.print("continue(y/n) : ");
        char isCon = kb.next().charAt(0);
        if(isCon=='y'){
            return true;
        }
        return false;
    }
    
    static void reTable(){
        for(int i=0; i<3; i++){
            for(int j=0; j<3; j++){
                table[i][j] = '-';
            }
        }
    }
    
    public static void main(String[] args) {
        printWelcome();
        while(true){
            while(true){
                printTable();
                printTurn();
                inputRowCol();
                if(isWin()){
                    printTable();
                    showResult();
                    break;
                }
                if(isDraw()){
                    printTable();
                    showResult();
                    break;
                }
                switchPlayer();
            }
            if(inputContinue()){
                reTable();
            }else{
                break;
            }
        }
    }
}
